package info.androidhive.sqlite.database;
/**
 * Класс реализующий работу с базой данных
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import info.androidhive.sqlite.database.model.Note;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 3;

    private static final String DATABASE_NAME = "films";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Note.CREATE_TABLE);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Files.copy(Paths.get("database//films.db"), Paths.get(db.getPath()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Note.TABLE_NAME);
        onCreate(db);
    }

    /**
     * Возвращает Id записи вставленной в базу данных
     *
     * @param name имя
     * @param genre жанр
     * @param year год
     * @param storage носитель
     * @param country страна
     * @param lenght длительность
     * @param comment комментарий
     * @return Id
     */
    public long insertNote(String name, String genre, int year, String storage, String country, int lenght, String comment) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Note.COLUMN_NAME, name);
        values.put(Note.COLUMN_GENRE, genre);
        values.put(Note.COLUMN_YEAR, year);
        values.put(Note.COLUMN_STORAGE, storage);
        values.put(Note.COLUMN_COUNTRY, country);
        values.put(Note.COLUMN_LENGHT, lenght);
        values.put(Note.COLUMN_COMMENT, comment);
        long id = db.insert(Note.TABLE_NAME, null, values);
        db.close();
        return id;
    }

    /**
     * Возвращает обьект-запись по указанному Id
     *
     * @param id id
     * @return запись
     */
    public Note getNote(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(Note.TABLE_NAME,
                null, Note.COLUMN_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Note note = new Note(
                cursor.getInt(cursor.getColumnIndex(Note.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_GENRE)),
                cursor.getInt(cursor.getColumnIndex(Note.COLUMN_YEAR)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_STORAGE)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_COUNTRY)),
                cursor.getInt(cursor.getColumnIndex(Note.COLUMN_LENGHT)),
                cursor.getString(cursor.getColumnIndex(Note.COLUMN_COMMENT))
        );
        cursor.close();

        return note;
    }

    /**
     * Возвращает список записей из базы данных
     *
     * @return список записей
     */
    public List<Note> getAllNotes() {
        List<Note> notes = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + Note.TABLE_NAME + " ORDER BY " +
        Note.COLUMN_PHOTO + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_ID)));
                note.setName(cursor.getString(cursor.getColumnIndex(Note.COLUMN_NAME)));
                note.setGenre(cursor.getString(cursor.getColumnIndex(Note.COLUMN_GENRE)));
                note.setYear(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_YEAR)));
                note.setStorage(cursor.getString(cursor.getColumnIndex(Note.COLUMN_STORAGE)));
                note.setCountry(cursor.getString(cursor.getColumnIndex(Note.COLUMN_COUNTRY)));
                note.setLenght(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_LENGHT)));
                note.setComment(cursor.getString(cursor.getColumnIndex(Note.COLUMN_COMMENT)));
                note.setPhoto(cursor.getString(cursor.getColumnIndex(Note.COLUMN_PHOTO)));
                notes.add(note);
            } while (cursor.moveToNext());
        }
        db.close();
        return notes;
    }

    /**
     * Обновляет запись
     *
     * @param note запись
     */
    public void updateNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Note.COLUMN_NAME, note.getName());
        values.put(Note.COLUMN_GENRE, note.getGenre());
        values.put(Note.COLUMN_YEAR, note.getYear());
        values.put(Note.COLUMN_STORAGE, note.getStorage());
        values.put(Note.COLUMN_COUNTRY, note.getCountry());
        values.put(Note.COLUMN_LENGHT, note.getLenght());
        values.put(Note.COLUMN_COMMENT, note.getComment());
        values.put(Note.COLUMN_PHOTO, note.getPhoto());

        db.update(Note.TABLE_NAME, values, Note.COLUMN_ID + "=?",
                new String[]{Integer.toString(note.getId())});
    }

    /**
     * Удаляет запись
     *
     * @param note запись
     */
    public void deleteNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Note.TABLE_NAME, Note.COLUMN_ID + " =?",
                new String[]{String.valueOf(note.getId())});
        db.close();
    }
}