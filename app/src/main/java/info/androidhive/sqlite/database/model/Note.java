package info.androidhive.sqlite.database.model;

/**
 * Класс реализующий работу обьекта-записи
 */
public class Note {
    public static final String TABLE_NAME = "films";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_GENRE = "genre";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_STORAGE = "storage";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_LENGHT = "lenght";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_PHOTO = "photo";

    private int id;
    private String name;
    private String genre;
    private int year;
    private String storage;
    private String country;
    private int lenght;
    private String comment;
    private String photo;

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + COLUMN_NAME + " TEXT,"
                    + COLUMN_GENRE + " TEXT,"
                    + COLUMN_YEAR + " INTEGER,"
                    + COLUMN_STORAGE + " TEXT,"
                    + COLUMN_COUNTRY + " TEXT,"
                    + COLUMN_LENGHT + " INTEGER,"
                    + COLUMN_COMMENT + " TEXT,"
                    + COLUMN_PHOTO + " TEXT"
                    + ")";

    public Note() {
    }

    public Note(int id, String name, String genre, int year, String storage, String country, int lenght, String comment) {
        this.id = id;
        this.name = name;
        this.genre = genre;
        this.year = year;
        this.storage = storage;
        this.country = country;
        this.lenght = lenght;
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getStorage() {
        return storage;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", year=" + year +
                ", storage='" + storage + '\'' +
                ", country='" + country + '\'' +
                ", lenght=" + lenght +
                ", comment='" + comment + '\'' +
                ", photo='" + photo + '\'' +
                '}';
    }
}