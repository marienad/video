package info.androidhive.sqlite.view;
/**
 * Класс описывающий интерфес активности фильма
 */

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.video.R;

import info.androidhive.sqlite.database.model.Note;

public class FilmActivity extends AppCompatActivity {
    Note note;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);
        note = MainActivity.films.get(getIntent().getIntExtra("position", 0));
        TextView name = findViewById(R.id.text_name);
        TextView genre = findViewById(R.id.text_genre);
        TextView year = findViewById(R.id.text_year);
        TextView storage = findViewById(R.id.text_storage);
        TextView country = findViewById(R.id.text_country);
        TextView lenght = findViewById(R.id.text_lenght);
        TextView comment = findViewById(R.id.text_comment);
        image = findViewById(R.id.image);
        try {
            image.setImageURI(Uri.parse(note.getPhoto()));
        } catch (NullPointerException e) {
            image.setImageResource(R.drawable.ic_launcher_background);
        }
        name.setText(note.getName());
        genre.setText("Жанр: " + note.getGenre());
        year.setText("Год выпуска: " + Integer.toString(note.getYear()));
        storage.setText("Носитель: " + note.getStorage());
        country.setText("Страна: " + note.getCountry());
        lenght.setText("Длительность(мин): " + Integer.toString(note.getLenght()));
        comment.setText("Комментарий: " + note.getComment());
        if (LoginActivity.ID > 0) {
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    startActivityForResult(intent, 1);
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Uri filePath = data.getData();
            note.setPhoto(filePath.toString());
            MainActivity.db.updateNote(note);
            MainActivity.films = MainActivity.db.getAllNotes();
            MainActivity.mAdapter.notifyItemChanged(getIntent().getIntExtra("position", 0));
            image.setImageURI(Uri.parse(note.getPhoto()));
        } catch (NullPointerException e) {
            return;
        }
    }
}
