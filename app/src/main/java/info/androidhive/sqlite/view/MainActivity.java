package info.androidhive.sqlite.view;

/**
 * Класс описывающий интерфес главного окна
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.video.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.sqlite.database.DatabaseHelper;
import info.androidhive.sqlite.database.model.Note;
import info.androidhive.sqlite.utils.MyDividerItemDecoration;
import info.androidhive.sqlite.utils.RecyclerTouchListener;

public class MainActivity extends AppCompatActivity {
    public static NotesAdapter mAdapter;
    private List<Note> notesList = new ArrayList<>();
    private List<Note> listFiltered;
    public static List<Note> films = new ArrayList<>();
    private RecyclerView recyclerView;

    public static DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler_view);
        db = new DatabaseHelper(this);
        notesList = db.getAllNotes();
        films = notesList;
        mAdapter = new NotesAdapter(films);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);
        FloatingActionButton fab = findViewById(R.id.fab);
        if (LoginActivity.ID == 0) {
            fab.setVisibility(View.INVISIBLE);
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                    recyclerView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, final int position) {
                    Intent intent = new Intent(getApplicationContext(), FilmActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("position", position);
                    startActivity(intent);
                }

                @Override
                public void onLongClick(View view, int position) {
                }
            }
            ));
        } else {

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showNoteDialog(false, null, -1);
                }
            });
            recyclerView.addOnItemTouchListener(new

                    RecyclerTouchListener(this,
                    recyclerView, new RecyclerTouchListener.ClickListener() {
                @Override
                public void onClick(View view, final int position) {
                    Intent intent = new Intent(getApplicationContext(), FilmActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("position", position);
                    startActivity(intent);
                }

                @Override
                public void onLongClick(View view, int position) {
                    showActionsDialog(position);
                }
            }));
        }
        final Button filrtrbtn = findViewById(R.id.filtr_button);
        filrtrbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filrtr();
            }
        });
    }

    private void filrtr() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        final View view = layoutInflaterAndroid.inflate(R.layout.filtr_form, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setView(view);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Применить", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {

                    }
                })
                .setNegativeButton("назад",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });
        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();
        final SeekBar year = view.findViewById(R.id.seekBarYear);
        final TextView tyear = view.findViewById(R.id.year_bar);
        final SeekBar lenght = view.findViewById(R.id.seekBarLenght);
        final TextView tlenght = view.findViewById(R.id.lenght_bar);
        year.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tyear.setText("Год: " + year.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        lenght.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tlenght.setText("Продолжительность до: " + lenght.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup genre = view.findViewById(R.id.genre_group);
                RadioButton genreb = view.findViewById(genre.getCheckedRadioButtonId());
                String selectQuery = "SELECT  * FROM " + Note.TABLE_NAME + " WHERE ";
                int a = 0;
                if (!genreb.getText().equals("все")) {
                    selectQuery += " genre='" + genreb.getText().toString().trim() + "'";
                    a++;
                }
                if (year.getProgress() > 1960) {
                    if (a > 0) {
                        selectQuery += " AND ";
                    }
                    selectQuery += " year=" + String.valueOf(year.getProgress());
                    a++;
                }
                if (lenght.getProgress() > 0) {
                    if (a > 0) {
                        selectQuery += " AND ";
                    }
                    selectQuery += " lenght<=" + String.valueOf(lenght.getProgress());
                    a++;
                }
                System.out.println(selectQuery);
                if (a == 0) {
                    films = notesList;
                } else {
                    List<Note> filterlist = new ArrayList<>();
                    db = new DatabaseHelper(getApplicationContext());
                    SQLiteDatabase dbd = db.getReadableDatabase();
                    Cursor cursor = dbd.rawQuery(selectQuery, null);
                    if (cursor.moveToFirst()) {
                        do {
                            Note note = new Note();
                            note.setId(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_ID)));
                            note.setName(cursor.getString(cursor.getColumnIndex(Note.COLUMN_NAME)));
                            note.setGenre(cursor.getString(cursor.getColumnIndex(Note.COLUMN_GENRE)));
                            note.setYear(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_YEAR)));
                            note.setStorage(cursor.getString(cursor.getColumnIndex(Note.COLUMN_STORAGE)));
                            note.setCountry(cursor.getString(cursor.getColumnIndex(Note.COLUMN_COUNTRY)));
                            note.setLenght(cursor.getInt(cursor.getColumnIndex(Note.COLUMN_LENGHT)));
                            note.setComment(cursor.getString(cursor.getColumnIndex(Note.COLUMN_COMMENT)));
                            note.setPhoto(cursor.getString(cursor.getColumnIndex(Note.COLUMN_PHOTO)));
                            filterlist.add(note);
                        } while (cursor.moveToNext());
                    }
                    db.close();
                    films = filterlist;
                }
                recyclerView.setAdapter(new NotesAdapter(films));
                alertDialog.dismiss();
            }
        });
    }

    private void deleteNote(int position) {
        db.deleteNote(films.get(position));
        films.remove(position);
        mAdapter.notifyDataSetChanged();
    }

    private void showActionsDialog(final int position) {
        CharSequence colors[] = new CharSequence[]{"Изменить", "Удалить"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Выбрать");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    showNoteDialog(true, films.get(position), position);
                } else {
                    deleteNote(position);
                }
            }
        });
        builder.show();
    }

    private void showNoteDialog(final boolean shouldUpdate, final Note note,
                                final int position) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getApplicationContext());
        View view = layoutInflaterAndroid.inflate(R.layout.activity_form, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilderUserInput.setView(view);

        final EditText inputName = view.findViewById(R.id.name);
        final EditText inputGenre = getViewById(view);
        final EditText inputYear = view.findViewById(R.id.year);
        final EditText inputStorage = view.findViewById(R.id.storage);
        final EditText inputCountry = view.findViewById(R.id.country);
        final EditText inputLenght = view.findViewById(R.id.lenght);
        final EditText inputComment = view.findViewById(R.id.comment);
        TextView dialogTitle = view.findViewById(R.id.dialog_title);
        dialogTitle.setText(!shouldUpdate ? getString(R.string.lbl_new_note_title) : getString(R.string.lbl_edit_note_title));

        if (shouldUpdate && note != null) {
            inputName.setText(note.getName());
            inputGenre.setText(note.getGenre());
            inputYear.setText(Integer.toString(note.getYear()));
            inputStorage.setText(note.getStorage());
            inputCountry.setText(note.getCountry());
            inputLenght.setText(Integer.toString(note.getLenght()));
            inputComment.setText(note.getComment());
        }
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(shouldUpdate ? "обновить" : "сохранить", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                    }
                })
                .setNegativeButton("назад",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(inputLenght.getText().toString()) || TextUtils.isEmpty(inputName.getText().toString()) ||
                        TextUtils.isEmpty(inputGenre.getText().toString()) ||
                        TextUtils.isEmpty(inputStorage.getText().toString()) ||
                        TextUtils.isEmpty(inputCountry.getText().toString()) || TextUtils.isEmpty(inputComment.getText().toString())) {
                    Toast.makeText(MainActivity.this, "Заполните все поля!", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    if (!inputYear.getText().toString().matches("\\d+")) {
                        inputYear.setText("");
                        Toast.makeText(MainActivity.this, "Введите год корректно", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        alertDialog.dismiss();
                    }
                    if (!inputLenght.getText().toString().matches("\\d+")) {
                        inputLenght.setText("");
                        Toast.makeText(MainActivity.this, "Введите длительность корректно", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        alertDialog.dismiss();
                    }
                }
                if (shouldUpdate && note != null) {
                    updateNote(note, inputName.getText().toString(), inputGenre.getText().toString(), Integer.parseInt(inputYear.getText().toString()),
                            inputStorage.getText().toString(), inputCountry.getText().toString(), Integer.parseInt(inputLenght.getText().toString()), inputComment.getText().toString(), note.getId());
                } else {
                    createNote(inputName.getText().toString(), inputGenre.getText().toString(), Integer.parseInt(inputYear.getText().toString()),
                            inputStorage.getText().toString(), inputCountry.getText().toString(), Integer.parseInt(inputLenght.getText().toString()), inputComment.getText().toString());
                }
            }
        });
    }

    private EditText getViewById(View view) {
        return view.findViewById(R.id.genre);
    }

    /**
     * Добавляет запись в список
     *
     * @param name имя
     * @param genre жанр
     * @param year год
     * @param storage носитель
     * @param country страна
     * @param lenght длительность
     * @param comment комментарий
     */
    public void createNote(String name, String genre, int year, String storage, String country,
                           int lenght, String comment) {
        int id = (int) db.insertNote(name, genre, year, storage, country, lenght, comment);
        Note note = db.getNote(id);
        if (note != null) {
            films.add(note);
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Обновляет запись в списке
     *
     * @param note запись
     * @param name имя
     * @param genre жанр
     * @param year год
     * @param storage носитель
     * @param country страна
     * @param lenght длительность
     * @param comment комментарий
     * @param position позиция
     */
    public void updateNote(Note note, String name, String genre, int year, String storage, String country,
                           int lenght, String comment, int position) {
        note.setName(name);
        note.setGenre(genre);
        note.setYear(year);
        note.setStorage(storage);
        note.setCountry(country);
        note.setLenght(lenght);
        note.setComment(comment);
        db.updateNote(note);
        notesList = db.getAllNotes();
        films = notesList;
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Ищет запись в списке
     *
     * @param view view
     */
    public void search(View view) {
        EditText editText = findViewById(R.id.search);
        String charString = editText.getText().toString();
        if (films.isEmpty()) {
            return;
        }
        if (charString.equals("")) {
            films = notesList;
        } else {
            List<Note> filteredList = new ArrayList<>();
            for (Note note : films) {
                if (note.getName().toLowerCase().contains(charString.toLowerCase())) {
                    filteredList.add(note);
                }
            }
            listFiltered = filteredList;
            films = listFiltered;
        }
        recyclerView.setAdapter(new NotesAdapter(films));
    }
}