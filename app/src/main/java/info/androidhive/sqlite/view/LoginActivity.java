package info.androidhive.sqlite.view;
/**
 * Класс описывающий интерфес активности входа
 */

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.video.R;

public class LoginActivity extends AppCompatActivity {
    public static int ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) !=
                        PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1000);
        }
    }

    /**
     * Реализует вход в приложение
     *
     * @param view view
     */
    public void login(View view) {
        final EditText inputName = findViewById(R.id.name);
        if (TextUtils.isEmpty(inputName.getText().toString())) {
            Toast.makeText(LoginActivity.this, "Введите имя", Toast.LENGTH_SHORT).show();
            return;
        }
        RadioButton button = findViewById(R.id.read);
        if (button.isChecked()) {
            ID = 0;
        } else {
            ID = 1;
        }
        Toast toast = Toast.makeText(LoginActivity.this, "Добро пожаловать " + inputName.getText().toString(), Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
